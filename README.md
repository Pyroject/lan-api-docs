# LAN-API-DOCS
API Documents

## Thông tin về thiết bị

Thiết bị đã được thiết lập IP tĩnh

```
    IP: 192.168.1.184
    Default Gateway: 192.168.1.1
    Subnet Mask: 255.255.0.0
    Primary DNS: 8.8.8.8
    Secondary DNS: 8.8.8.4
```
Địa chỉ MAC: ``A4:CF:12:58:E2:47``

``Chú ý: Trường hợp thiết bị không tự nhận IP trên (có thể trong mạng đã có thiết bị được thiết lập IP tương tự), Một cách an toàn bạn có thể đăng nhập vào Router để thiết lập địa chỉ IP tĩnh cho thiết bị này dựa trên địa chỉ MAC được cung cấp.``

## API

``Sử dụng giao thức HTTP``

### Request:

```
    Url: http://192.168.1.184/control
    Method: POST
    Content-Type: application/json
    Body:
    {
        "PASSWORD": "PYROJECTCOLTD",
        "CMD": CMD_STRING
    }
```

Thông tin về CMD_STRING

```
    "ALLOW": Bật đèn xanh, tắt đèn đỏ.
    "STOP" : Tắt đèn xanh, bật đèn đỏ.
    "OFF"  : Tắt cả 2 đèn. 
```

### Respone
```
    {
        "RESCODE": code
    }
```

Thông tin về mã code

```
    1000: Thành công
    2000: Sai mật khẩu
    2001: Lỗi Params không đúng
    2002: Lỗi Server
```

### Ví dụ

#### Bật đèn đỏ và tắt đèn xanh

Request:

```
    Url: http://192.168.1.184/control
    Method: POST
    Content-Type: application/json
    Body:
    {
        "PASSWORD": "PYROJECTCOLTD",
        "CMD": "STOP"
    }
```

Respone (thành công):

```
    {
        "RESCODE": 1000 
    }
```
#### Bật đèn xanh và tắt đèn đỏ
Request:

```
    Url: http://192.168.1.184/control
    Method: POST
    Content-Type: application/json
    Body:
    {
        "PASSWORD": "PYROJECTCOLTD",
        "CMD": "ALLOW"
    }
```

Respone (thành công):

```
    {
        "RESCODE": 1000 
    }
```
#### Tắt đèn xanh và tắt đèn đỏ
Request:

```
    Url: http://192.168.1.184/control
    Method: POST
    Content-Type: application/json
    Body:
    {
        "PASSWORD": "PYROJECTCOLTD",
        "CMD": "OFF"
    }
```

Respone (thành công):

```
    {
        "RESCODE": 1000 
    }
```

#### Bật đèn xanh và tắt đèn đỏ

Request:

```
    Url: http://192.168.1.184/control
    Method: POST
    Content-Type: application/json
    Body:
    {
        "PASSWORD": "PYROJECTCOLTD",
        "CMD": "STOP_AND_STOP???"
    }
```

Respone (Lỗi Params không đúng):

```
    {
        "RESCODE": 2001
    }
```
